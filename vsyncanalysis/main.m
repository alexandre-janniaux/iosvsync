//
//  main.m
//  vsyncanalysis
//
//  Created by Alexandre Janniaux on 27/10/2021.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

//
//  ViewController.m
//  vsyncanalysis
//
//  Created by Alexandre Janniaux on 27/10/2021.
//

#import <UIKit/UIKit.h>
#import <OpenGLES/EAGL.h>
#import <OpenGLES/ES2/gl.h>
#import <OpenGLES/ES2/glext.h>
#import <QuartzCore/QuartzCore.h>

@interface GLWindow : UIView {
    CAEAGLLayer *_layer;
}
@end

@implementation GLWindow
+ (Class)layerClass {
    return [CAEAGLLayer class];
}
@end

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    UIWindow *_window;
    GLWindow *_view;
    EAGLContext *_context;
    CADisplayLink *_displayLink;

    GLuint _framebuffer;
    GLuint _renderbuffer;

    CFTimeInterval _lastRender;

    const char *_renderMode, *_dropMode;

    long long unsigned _frameNo;
}
- (void)step:(CADisplayLink *)sender;
@end

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    // Do any additional setup after loading the view.

    _window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    _window.rootViewController = [UIViewController alloc];
    _window.backgroundColor = [UIColor blackColor];

    _view = [[GLWindow alloc] initWithFrame:_window.bounds];
    _context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES3];

    EAGLContext *previous = [EAGLContext currentContext];
    [EAGLContext setCurrentContext:_context];

    glGenRenderbuffers(1, &_renderbuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _renderbuffer);

    glGenFramebuffers(1, &_framebuffer);
    glBindFramebuffer(GL_FRAMEBUFFER, _framebuffer);
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                              GL_RENDERBUFFER, _renderbuffer);

    [_context renderbufferStorage:GL_RENDERBUFFER
                   fromDrawable:_view.layer];
    assert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
    [EAGLContext setCurrentContext:previous];

    [_window addSubview:_view];
    [_window makeKeyAndVisible];

    _lastRender = CACurrentMediaTime();
    _frameNo = 0;

    _renderMode = getenv("RENDER_MODE");
    if (_renderMode == NULL)
        _renderMode = "vsync";

    _dropMode = getenv("DROP_MODE");
    if (_dropMode == NULL)
        _dropMode = "once";

    if (!strcmp(_renderMode, "loop"))
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self periodicRender];
        });
    }
    else if (!strcmp(_renderMode, "vsync"))
    {
        _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(onVsync:)];
        [_displayLink addToRunLoop:[NSRunLoop mainRunLoop]
                           forMode:NSRunLoopCommonModes];
    }

    return YES;
}

- (void)periodicRender {
    [self render];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self periodicRender];
    });
}

- (void)onVsync:(CADisplayLink *)sender {
    [self render];
}

- (void)render {
    CFTimeInterval begin_context = CACurrentMediaTime();
    EAGLContext *previous = [EAGLContext currentContext];
    [EAGLContext setCurrentContext:_context];

    CFTimeInterval begin_clear = CACurrentMediaTime();
    glClearColor(cos(begin_context), 0.f, 0.f, 1.f);
    glClear(GL_COLOR_BUFFER_BIT);

    CFTimeInterval begin_present = CACurrentMediaTime();
    [_context presentRenderbuffer:GL_RENDERBUFFER];

    CFTimeInterval begin_release = CACurrentMediaTime();
    [EAGLContext setCurrentContext:previous];

    CFTimeInterval end_render = CACurrentMediaTime();

    if (begin_present - begin_clear > 5. / 1000 || true)
    {
        NSLog(@"RENDER: total=%.3lf   makeCurrent=%.3lf   clear=%.3lf   present=%.3lf   release=%.3lf",
              1000. * (end_render - begin_context),
              1000. * (begin_clear - begin_context),
              1000. * (begin_present - begin_clear),
              1000. * (begin_release - begin_present),
              1000. * (end_render - begin_release));
        NSLog(@"RENDER: FPS=%.3lf", 1.f / (begin_context - _lastRender));
    }
    _lastRender = begin_context;
    _frameNo++;
}
@end

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([AppDelegate class]);
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
